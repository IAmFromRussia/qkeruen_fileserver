﻿FROM golang:1.20.3-alpine AS builder
LABEL author="Danila" maintainer="Erek"

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /fileserver

FROM builder as test
RUN go test -v ./...

FROM alpine:latest AS stage

WORKDIR /

COPY --from=builder /fileserver /fileserver

EXPOSE 3001

USER nobody