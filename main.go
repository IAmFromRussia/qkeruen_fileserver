package main

import (
	"io"
	"net/http"
	"os"
	"path/filepath"
    "github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"fmt"
)


func main() {
	r := gin.Default()
    
	r.MaxMultipartMemory = 32 << 20 // 8 MiB
    avaDir := "./var/www/file/ava"
	docDir := "./var/www/file/document/"

	r.Use(gin.Recovery())
	//gin.SetMode(gin.ReleaseMode)
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "GET", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Content-Length", "Authorization", "X-CSRF-Token", "Content-Type", "Accept", "X-Requested-With", "Bearer", "Authority"},
		ExposeHeaders:    []string{"Content-Length", "Authorization", "Content-Type", "application/json", "Content-Length", "Accept-Encoding", "X-CSRF-Token",  "Accept", "Origin", "Cache-Control", "X-Requested-With"},
		AllowCredentials: true,
		AllowOriginFunc: func(origin string) bool {
			return origin == "https://s3.qkeruen.kz"
		},
		//MaxAge: 12 * time.Hour,
	}))

	

	// File upload endpoint
	r.POST("/ava/upload/:fileName", func(c *gin.Context) {
		fileName := c.Param("fileName")
		// Retrieve the file from the form data
		file, err := c.FormFile("file")
		
		if err != nil {
			c.String(http.StatusBadRequest, err.Error())
			return
		}

		// Create the upload directory if it doesn't exist
		if err := os.MkdirAll(avaDir, os.ModePerm); err != nil {
			c.String(http.StatusInternalServerError, "Failed to create upload directory")
			return
		}

		// Save the file to the specified directory
		filePath := filepath.Join(avaDir, fileName)
		if err := c.SaveUploadedFile(file, filePath); err != nil {
			c.String(http.StatusInternalServerError, "Failed to save the file")
			return
		}

		c.String(http.StatusOK, "File uploaded successfully")
	})

	r.POST("/doc/upload/:fileNames", func(c *gin.Context) {
		fileName := c.Param("fileNames")
		
		// Retrieve the file from the form data
		file, err := c.FormFile("file")
		fmt.Println(err.Error())
		if err != nil {
			c.String(http.StatusBadRequest, "Bad request")
			return
		}

		// Create the upload directory if it doesn't exist
		if err := os.MkdirAll(docDir, os.ModePerm); err != nil {
			c.String(http.StatusInternalServerError, "Failed to create upload directory")
			return
		}

		// Save the file to the specified directory
		filePath := filepath.Join(docDir, fileName)
		if err := c.SaveUploadedFile(file, filePath); err != nil {
			c.String(http.StatusInternalServerError, "Failed to save the file")
			return
		}

		c.String(http.StatusOK, "File uploaded successfully")
	})

	// File download endpoint
	r.POST("/ava/download/:filename", func(c *gin.Context) {
		filename := c.Param("filename")
		
		filePath := filepath.Join(avaDir, filename)

		// Check if the file exists
		_, err := os.Stat(filePath)
		if os.IsNotExist(err) {
			c.String(http.StatusNotFound, "File not found")
			return
		}

		// Open the file
		file, err := os.Open(filePath)
		if err != nil {
			c.String(http.StatusInternalServerError, "Internal server error")
			return
		}
		defer file.Close()

		// Set the appropriate headers
		c.Header("Content-Disposition", "attachment; filename="+filename)
		c.Header("Content-Type", "application/octet-stream")
		c.Header("Content-Transfer-Encoding", "binary")

		// Stream the file to the client
		io.Copy(c.Writer, file)
	})

	r.POST("/doc/download/:filename", func(c *gin.Context) {
		filename := c.Param("filename")

		filePath := filepath.Join(docDir, filename)

		// Check if the file exists
		_, err := os.Stat(filePath)
		if os.IsNotExist(err) {
			c.String(http.StatusNotFound, "File not found")
			return
		}

		// Open the file
		file, err := os.Open(filePath)
		if err != nil {
			c.String(http.StatusInternalServerError, "Internal server error")
			return
		}
		defer file.Close()

		// Set the appropriate headers
		c.Header("Content-Disposition", "attachment; filename="+filename)
		c.Header("Content-Type", "application/octet-stream")
		c.Header("Content-Transfer-Encoding", "binary")

		// Stream the file to the client
		io.Copy(c.Writer, file)
	})

	r.Run(":3001")
}
